import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import { v4 as uuid } from 'uuid'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    issues: [
      {
        id: 1,
        description: 'this is test description 1',
        open: true,
        done: false,
        trashed: false
      },
      {
        id: 2,
        description: 'this is test description 2',
        open: true,
        done: false,
        trashed: false
      },
      {
        id: 3,
        description: 'this is test description 3',
        open: false,
        done: true,
        trashed: false
      },
      {
        id: 4,
        description: 'this is test description 4',
        open: false,
        done: true,
        trashed: false
      },
      {
        id: 5,
        description: 'this is test description 5',
        open: false,
        done: false,
        trashed: true
      }
    ]
  },
  mutations: {
    addNewIssue: (state, newIssue) => (state.issues.push(newIssue)),
    markIssueTrashed: (state, id) => {
      state.issues.find((issue) => {
        if (issue.id === id) {
          issue.trashed = !issue.trashed
          issue.open = false
          issue.done = false
        }
      })
    },
    markIssueDone: (state, id) => {
      state.issues.find((issue) => {
        if (issue.id === id) {
          issue.done = !issue.done
          issue.trashed = false
          issue.open = false
        }
      })
    },
    markIssueOpen: (state, id) => {
      state.issues.find((issue) => {
        if (issue.id === id) {
          issue.open = !issue.open
          issue.trashed = false
          issue.done = false
        }
      })
    },
    updateIssueMutation: (state, updatedIssue) => {
      state.issues.find((issue) => {
        if (issue.id === updatedIssue.id) {
          issue.description = updatedIssue.description
        }
      })
    }
  },
  actions: {
    addIssue ({ commit }, description) {
      const newIssue = {
        id: uuid(),
        description: description,
        done: false,
        trashed: false,
        open: true
      }
      commit('addNewIssue', newIssue)
    },
    markTrashed ({ commit }, id) {
      commit('markIssueTrashed', id)
    },
    markDone ({ commit }, id) {
      commit('markIssueDone', id)
    },
    markOpen ({ commit }, id) {
      commit('markIssueOpen', id)
    },
    updateIssue ({ commit }, updatedIssue) {
      commit('updateIssueMutation', updatedIssue)
    }
  },
  getters: {
    allIssues: (state) => state.issues
  },
  modules: {
  },
  plugins: [new VuexPersistence().plugin]
})
