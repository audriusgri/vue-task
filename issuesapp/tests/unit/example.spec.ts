import { shallowMount, mount } from '@vue/test-utils'
import HelloWorld from '@/components/HelloWorld.vue'
// import Open from '../views/Open.vue'
import Vue from 'vue'

describe('HelloWorld.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'Click here to open Open issues'
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg }
    })
    expect(wrapper.text()).toMatch(msg)
  })
})

// describe('Open.vue', () => {
//   it('Open issues tab should be visible', () => {
//     const wrapper = mount(Open)
//     const text = wrapper.find('h1')
//     expect(text.text()).toContain('Open issues')
//   })
// })

describe('HelloWorld.vue', () => {
  it('expect to see that p has link to open open issues', () => {
    const wrapper = mount(HelloWorld)
    const text = wrapper.find('p')
    expect(text.text()).toContain('Click here to open Open issues')
  })
})
